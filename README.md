# Presenty Box PHP

Welcome to Presenty Box PHP, the easy-to-implement masonry gallery and lightbox carousel plugin for PHP websites. Implementation of this plugin is straight forward and includes the following features for easy implementation:

 1. Implementation in only five lines of code
 2. Dynamic, on-the-fly image thumbnail creation and caching to ensure only small thumbnail sized images are used in the gallery.
 3. Easily adjust hover call to action text content
 4. Easily adjust positioning of image title and alt, as well as the gallery call to action

## Requirements

To get started, you will need to ensure that you have the following prerequisets:

 - GD Image Library installed on the sever. (If not, you will need to manually create thumbnails)
 - JQuery library.
 - PHP 7.0

## Implementation

 1. Download the files from BitBucket using either download or clone.
 2. Within the **presenty-box-php** folder, you will find three folders: **css**, **js**, and **inc**. Copy the **presenty-box** folder within each of the listed folders to their corresponding folders on the website. (It doesn't matter what the folder names are, simply that you have folders to place the CSS files, JS files and Presenty-Box dependancies.)
 3. Add the following code to your showcase or gallery page right before the ending **/HEAD** tag as follows:
 
    include_once($_SERVER['DOCUMENT_ROOT']  .  '/inc/presentybox/presentybox.php');
    $pbox =  new  presentybox(CSSdir, JSDir, Colour, CTAText, GalleryFolder, TextPosition, CTAPosition,ThumbnailWidth, ThumbnailHeight, ErrorReporting);
    echo $pbox->styles();

4. Adjust the presentybox call and adjust the values accordingly:
    1. **CSSdir**: The directory where the **presenty-box** css folder is located. Values should include a forward slash in the front. (ex. "/css").
    2. **JSDir**: The directory where the **presenty-box** js folder is located. Values should include a forward slash in the front, (ex. "/js");
    3. **Colour**: The background colour of the hover overlay. This is a solid colour in hex (ex. "#00FF00").
    4. **CTA Text**: The call to action text placed on each gallery item, (ex. "VIEW").
    5. **GalleryFolder**: The folder where all of the current gallery images are located. This can be unique for each gallery page so that gallery images are organized into their respective galleries, (ex. "/gallery-images-1").
    6. **Text Position**: The position of the image title, and description. Values include, "TopLeft", "TopRight", "Center", "BottomLeft", "BottomRight".
    7. **CTA Position**: The position of the call to action text.  Values include, "TopLeft", "TopRight", "Center", "BottomLeft", "BottomRight".
    8. **Thumbnail Width**: The maximum width of the masonry gallery thumbnails, (ex. 500).
    9. **Thumbnail Height**: The maximum height of the masonry gallery thumbnails, (ex 350).
    10.**Error Reporting**: Will enable error reporting for diagnosting purposes, (ex. true). 
5. The resulting code should appear similar to the following:

    $pbox =  new  presentybox('/css','/js','#000000','View More','/showcase-item-images','Center','BottomRight',500,350);

6. Place the following gallery output code in the body of your document. This will output the gallery HTML whereever it is placed. 

    <?php $pbox->outputGallery(); ?>

7. Place the following gallery script code before the closing **/BODY** in the HTML structure of your document.

    <?php  echo $pbox->scripts(); ?>

8. Now you will need to create the gallery folder in the same directory as the page you have just edited. This folder must have the same name that you have specified for **GalleryFolder** above. 
9. Before uploading the images for this gallery, resize and compress them using compression software or Adobe Photoshop. The dimensions should be for the full scale images, and not thumbnail images. Once this has been done, upload the images to the **GalleryFolder**
10. Run the script by viewing the new gallery page. If everything wors, the script will dynamically create thumbail images based on the **Thumbnail Width** and **Thumbnail Height** dimensions. 
11. To make CSS changes, simply locate the **custom-styles.css* file under the **CSS Dir**/presenty-box/custom-styles.css. Complete any alterations to presenty-box here.
12. Shake and dance. 

