<?php

/**
 * 
 * presentybox v1.0.0
 * 
 * Developed by Aaron Kostka Smith, FirstPage Marketing
 * 
 * 
 */

class presentybox { 

    public $cssdir = '/css';
    public $jsdir = '/js';
    public $bgcolour = '#ff0000';
    public $htmlcta = 'View';
    public $imageFolder = 'images/';
    public $textPosition = 'BottomRight';
    public $viewPosition = 'Center';
    public $thumbnailWidth = 500;
    public $thumbnailHeight = 350;
    public $errorReporting = false;

    private $images = array();
    private $thumbnailPath = 'thumbnails';

    /**
     * __construct
     *
     * @param  mixed $cssdir
     * @param  mixed $jsdir
     * @param  mixed $bgcolour
     * @param  mixed $htmlcta
     * @param  mixed $imageFolder
     * @param  mixed $textPosition
     * @param  mixed $viewPosition
     * @param  mixed $thumbnailWidth
     * @param  mixed $thumbnailHeight
     * @param  mixed $error
     *
     * @return void
     */
    public function __construct($cssdir = null, $jsdir = null,$bgcolour = null,$htmlcta = null,$imageFolder = null,$textPosition = null,$viewPosition = null,$thumbnailWidth = null, $thumbnailHeight = null, $error = null) {
        if ($cssdir) $this->$cssdir = $cssdir;
        if ($jsdir) $this->jsdir = $jsdir;
        if ($bgcolour) $this->bgcolour = $bgcolour;
        if ($htmlcta) $this->$htmlcta = $htmlcta;
        if ($imageFolder) $this->imageFolder = $imageFolder;
        if ($textPosition) $this->textPosition = $textPosition;
        if ($viewPosition) $this->viewPosition = $viewPosition;
        if ($errorReporting) $this->errorReporting = $errorReporting;

        if ($this->textPosition == $this->viewPosition) { echo 'Text position and view position must be different values for PresentyBox'; exit(); }
    } 

    /**
     * error
     *
     * @return void
     */
    private function error() {
        if ($this->errorReporting) {
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
        }
    }

    /**
     * getImages
     *
     * @return void
     */
    private function getImages() {
        if ($this->makeThumbnailFolder()) {
            $directory = $this->imageFolder;
            $handle = opendir($_SERVER['DOCUMENT_ROOT'].$directory);
            while($file = readdir($handle)){
                if($file !== '.' && $file !== '..' && $file !== $this->thumbnailPath){
                    if ($result = $this->checkExisting($_SERVER['DOCUMENT_ROOT'].$directory,$file)) {
                        $this->images[] = $file;
                    }
                }
            }    
        }        
    }

    /**
     * makeThumbnailFolder
     *
     * @return void
     */
    private function makeThumbnailFolder() {
        $directory = $_SERVER['DOCUMENT_ROOT'].$this->imageFolder;
        if (!is_dir($directory.'/'.$this->thumbnailPath)) { 
            return mkdir ($directory.'/'.$this->thumbnailPath);
        } else {
            return true;
        }
    }

    /**
     * checkExisting
     *
     * @param  mixed $directory
     * @param  mixed $image
     *
     * @return void
     */
    private function checkExisting($directory,$image) {
        if (!file_exists($directory."/".$this->thumbnailPath."/".$image)) {
            return $this->createThumbnail($image,$this->thumbnailWidth,$this->thumbnailHeight,$directory,$directory."/".$this->thumbnailPath."/");
        } else {
            return true;
        }
    }


    /**
     * createThumbnail
     *
     * @param  mixed $imageName
     * @param  mixed $newWidth
     * @param  mixed $newHeight
     * @param  mixed $uploadDir
     * @param  mixed $moveToDir
     *
     * @return void
     */
    private function createThumbnail($imageName,$newWidth,$newHeight,$uploadDir,$moveToDir)
    {
        $path = $uploadDir . '/' . $imageName;

        $mime = getimagesize($path);

        if($mime['mime']=='image/png'){ $src_img = imagecreatefrompng($path); }
        if($mime['mime']=='image/jpg'){ $src_img = imagecreatefromjpeg($path); }
        if($mime['mime']=='image/jpeg'){ $src_img = imagecreatefromjpeg($path); }
        if($mime['mime']=='image/pjpeg'){ $src_img = imagecreatefromjpeg($path); }

        $old_x = imageSX($src_img);
        $old_y = imageSY($src_img);

        if($old_x > $old_y)
        {
            $thumb_w    =   $newWidth;
            $thumb_h    =   $old_y/$old_x*$newWidth;
        }

        if($old_x < $old_y)
        {
            $thumb_w    =   $old_x/$old_y*$newHeight;
            $thumb_h    =   $newHeight;
        }

        if($old_x == $old_y)
        {
            $thumb_w    =   $newWidth;
            $thumb_h    =   $newHeight;
        }

        $dst_img        =   ImageCreateTrueColor($thumb_w,$thumb_h);

        imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);


        // New save location
        $new_thumb_loc = $moveToDir . $imageName;

        if($mime['mime']=='image/png'){ $result = imagepng($dst_img,$new_thumb_loc,8); }
        if($mime['mime']=='image/jpg'){ $result = imagejpeg($dst_img,$new_thumb_loc,80); }
        if($mime['mime']=='image/jpeg'){ $result = imagejpeg($dst_img,$new_thumb_loc,80); }
        if($mime['mime']=='image/pjpeg'){ $result = imagejpeg($dst_img,$new_thumb_loc,80); }

        imagedestroy($dst_img);
        imagedestroy($src_img);
        return $result;
    }



    /**
     * styles
     *
     * @param  mixed $cssdir
     *
     * @return void
     */
    public function styles($cssdir = null) {
        if (!$cssdir) {
            $cssdir = $this->cssdir;
        }
        $k = '';
        $k .= "<link href='$cssdir/presentybox/photoswipe.css' rel='stylesheet'>\n\r";
        $k .= "<link href='$cssdir/presentybox/default-skin.css' rel='stylesheet'>\n\r";
        $k .= "<link href='$cssdir/presentybox/presentybox.css' rel='stylesheet'>\n\r";
        $k .= "<link href='$cssdir/presentybox/custom-styles.css' rel='stylesheet'>\n\r";

        $startColor = $this->hex2rgba($this->bgcolour,0.65);
        $endColor =  $this->hex2rgba($this->bgcolour,0.01);
        $midColor = $this->hex2rgba($this->bgcolour,0.4);
        
        $k .= "
        <style>
        body .masonry_gallery .masonry-item figcaption.TopLeft:before {                                 
            background: -moz-linear-gradient(-45deg, " . $startColor. " 0%, " . $endColor . " 100%);
            background: -webkit-linear-gradient(-45deg, " . $startColor. " 0%, " . $endColor . " 100%);
            background: linear-gradient(135deg,  " . $startColor. " 0%, " . $endColor . " 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#a6000000', endColorstr='#00000000', GradientType=1);
        }
        body .masonry_gallery .masonry-item figcaption.BottomLeft:before {                                 
            background: -moz-linear-gradient(45deg,  " . $startColor. " 0%, " . $endColor . " 100%);
            background: -webkit-linear-gradient(45deg,  " . $startColor. " 0%, " . $endColor . " 100%);
            background: linear-gradient(45deg,  " . $startColor. " 0%, " . $endColor . " 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#a6000000', endColorstr='#00000000', GradientType=1);
        }
        body .masonry_gallery .masonry-item figcaption.TopRight:before {                                 
            background: -moz-linear-gradient(45deg,  " . $endColor. " 0%, " . $startColor . " 100%);
            background: -webkit-linear-gradient(45deg, r " . $endColor. " 0%, " . $startColor . " 100%);
            background: linear-gradient(45deg,  " . $endColor. " 0%, " . $startColor . " 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#a6000000', endColorstr='#00000000', GradientType=1);
        }
        body .masonry_gallery .masonry-item figcaption.BottomRight:before {                                 
            background: -moz-linear-gradient(-45deg,  " . $endColor. " 0%, " . $startColor . " 100%);
            background: -webkit-linear-gradient(-45deg,  " . $endColor. " 0%, " . $startColor . " 100%);
            background: linear-gradient(135deg,  " . $endColor. " 0%, " . $startColor . " 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#a6000000', endColorstr='#00000000', GradientType=1);
        }
        body .masonry_gallery .masonry-item figcaption.Center:before {
            background: " . $midColor . ";
        }
        </style>\n\r
        ";

        return $k;
        
    }

    /**
     * scripts
     *
     * @param  mixed $jsdir
     *
     * @return void
     */
    public function scripts($jsdir = null) {
        if (!$jsdir) {
            $jsdir = $this->jsdir;
        }
        
        $k = '<script src="' . $jsdir . '/presentybox/photoswipe.min.js"></script>' . "\n\r";
        $k .= '<script src="' . $jsdir . '/presentybox/photoswipe-ui-default.min.js"></script>' . "\n\r";
        $k .= '<script src="' . $jsdir . '/presentybox/photoswipe-support.js"></script>' . "\n\r";   
        $k .= '<script src="' . $jsdir . '/presentybox/masonry.pkgd.min.js"></script>' . "\n\r";   

        $k .= "<script type='text/javascript'>
        jQuery(document).ready(function($) {
            $(window).load(function() {
                $('.masonry_gallery').masonry({
                    // options
                    itemSelector: '.masonry-item',
                    columnWidth: '.masonry-item',
                    percentPosition: true,
                    gutter: 10
                    });
                    initPhotoSwipeFromDOM('.masonry_gallery');
                    console.info('Masonry gallery loaded');
                });
                });
                </script>";
        $k .= $this->photoSwipe();
        return $k;
    }

    /**
     * outputGallery
     *
     * @return void
     */
    public function outputGallery() {
        $this->getImages();
        $gallery = '<div class="masonry_gallery">';
        
        foreach ( $this->images as $image ) {
         
            $abspath = $_SERVER['DOCUMENT_ROOT'].$this->imageFolder.'/'.$image;
            $fullsize = $this->imageFolder.'/'.$image;
            $thumbnail = $this->imageFolder.'/'.$this->thumbnailPath.'/'.$image;
            $size = getimagesize($abspath);
            $i = 'Temp';
            
            $gallery .= "

                <figure class='masonry-item' >
                    <a href=".$fullsize." itemprop='contentUrl' data-size='"  . $size[0] . "x" . $size[1] . "' >
                            <img src=".$thumbnail.">
                    </a>
                        
                    <figcaption itemprop='caption description' class='" . $this->textPosition . "'>
                        <div class='img-title'>".$i."<div class='img-caption'>".$i."</div></div>
                        <div class='cta-hover " . $this->viewPosition . "'><span>" . stripslashes($this->htmlcta) . "</span></div>    
                    </figcaption>
                </figure>
                ";
        }
        $gallery .= '</div>';
        
        echo $gallery;

    }

    /**
     * photoSwipe
     *
     * @return void
     */
    public function photoSwipe() {
        $k = '<!-- Root element of PhotoSwipe. Must have class pswp. -->
                    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

                        <!-- Background of PhotoSwipe.   Its a separate element as animating opacity is faster than rgba(). -->
                        <div class="pswp__bg"></div>

                        <!-- Slides wrapper with overflow:hidden. -->
                        <div class="pswp__scroll-wrap">

                            <!-- Container that holds slides. 
                                PhotoSwipe keeps only 3 of them in the DOM to save memory.
                                Dont modify these 3 pswp__item elements, data is added later on. -->
                            <div class="pswp__container">
                                <div class="pswp__item"></div>
                                <div class="pswp__item"></div>
                                <div class="pswp__item"></div>
                            </div>

                            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                            <div class="pswp__ui pswp__ui--hidden">

                                <div class="pswp__top-bar">

                                    <!--  Controls are self-explanatory. Order can be changed. -->

                                    <div class="pswp__counter"></div>

                                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                                    <button class="pswp__button pswp__button--share" title="Share"></button>

                                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                                    
                                    <!-- element will get class pswp__preloader--active when preloader is running -->
                                    <div class="pswp__preloader">
                                        <div class="pswp__preloader__icn">
                                        <div class="pswp__preloader__cut">
                                            <div class="pswp__preloader__donut"></div>
                                        </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                    <div class="pswp__share-tooltip"></div> 
                                </div>

                                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                                </button>

                                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                                </button>

                                <div class="pswp__caption">
                                    <div class="pswp__caption__center"></div>
                                </div>

                            </div>

                        </div>

                    </div>';
                    $k .= "\n\r";
        return $k;
    }

    /**
     * hex2rgba
     *
     * @param  mixed $color
     * @param  mixed $opacity
     *
     * @return void
     */
    public function hex2rgba($color, $opacity = false) {
    
        $default = 'rgb(0,0,0)';
    
        //Return default if no color provided
        if(empty($color))
            return $default; 
    
        //Sanitize $color if "#" is provided 
        if ($color[0] == '#' ) {
            $color = substr( $color, 1 );
        }

        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
                return $default;
        }

        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);

        //Check if opacity is set(rgba or rgb)
        if($opacity){
            if(abs($opacity) > 1)
                $opacity = 1.0;
            $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
            $output = 'rgb('.implode(",",$rgb).')';
        }

        //Return rgb(a) color string
        return $output;
    }



}