<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Presenty-Box</title>
    <!--- START PRESENTY BOX HEAD CODE --->
    <?php 
    include_once($_SERVER['DOCUMENT_ROOT'] . '/inc/presentybox/presentybox.php');
    $pbox = new presentybox('/css','/js','#000000','View More','/showcase-item-images','Center','BottomRight');
    echo $pbox->styles();
    ?>
    <!--- END PRESENTY BOX HEAD CODE --->
</head>
<body>
    <h1>Presenty Box Example</h1>
    <div class="container">
        <!--- START PRESENTY BOX GALLERY OUTPUT --->
        <?php $pbox->outputGallery(); ?>
        <!--- END PRESENTY BOX GALLERY OUTPUT --->
    </div>
</body>
<!--- START PRESENTY BOX FOOTER CODE --->
<?php echo $pbox->scripts(); ?>
<!--- END PRESENT BOX FOOTER CODE --->
</html>




